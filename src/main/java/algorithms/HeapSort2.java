package main.java.algorithms;

import main.java.data_structure.MaxHeap;

/**
 * @author Wrb
 * @date 2019/5/28 17:26
 */
public class HeapSort2 implements Sort {
	@Override
	public void sort(int[] arr, int n) {
		MaxHeap maxHeap = new MaxHeap(arr, n);
		for (int i = n - 1; i >= 0; i--) {
			arr[i] = maxHeap.extractMax();
		}
	}
}
