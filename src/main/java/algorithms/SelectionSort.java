package main.java.algorithms;

import main.java.utils.SortTestHelper;

/**
 * @author Wrb
 * @date 2019/5/22 14:58
 */
public class SelectionSort implements Sort{

	public void sort(int[] arr, int n) {
		for (int i = 0 ;i < n ; i++) {
			int minIndex = i;
			for (int j = i + 1; j < n; j++) {
				if (arr[j] < arr[minIndex]) {
					minIndex = j;
				}
			}
			SortTestHelper.swap(arr, i, minIndex);
		}
	}
}
