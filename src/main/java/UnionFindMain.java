package main.java;

import main.java.utils.UnionFindTestHelper;

/**
 * @author Wrb
 * @date 2019/6/12 16:22
 */
public class UnionFindMain {

	public static void main(String[] args) {
		int n = 1000000;
//		UnionFindTestHelper.testUF1(n);
//		UnionFindTestHelper.testUF2(n);
		UnionFindTestHelper.testUF3(n);
		UnionFindTestHelper.testUF4(n);
		UnionFindTestHelper.testUF5(n);

	}
}
