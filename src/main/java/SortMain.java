package main.java;

import main.java.algorithms.*;
import main.java.utils.SortTestHelper;


/**
 * @author Wrb
 * @date 2019/5/22 14:51
 */
public class SortMain {
	public static void main(String[] args) {
		int n = 100000;
		int arr[] = SortTestHelper.generateRandomArray(n, 0, n);
//		int arr[] = SortTestHelper.generateNealyOrderedArray(n, 10);
		int arr2[] = arr.clone();
		int arr3[] = arr.clone();
		int arr4[] = arr.clone();
		int arr5[] = arr.clone();
		int arr6[] = arr.clone();
		int arr7[] = arr.clone();
		int arr8[] = arr.clone();
		int arr9[] = arr.clone();
//		SortTestHelper.printArray(arr, n);
		SortTestHelper.testSort("SelectionSort", new SelectionSort(), arr, n);
		SortTestHelper.testSort("InsertionSort", new InsertionSort(), arr2, n);
		SortTestHelper.testSort("MergeSort", new MergeSort(), arr3, n);
		SortTestHelper.testSort("QuickSort", new QuickSort(), arr4, n);
		SortTestHelper.testSort("QuickSort2", new QuickSort2(), arr5, n);
		SortTestHelper.testSort("QuickSort3", new QuickSort3(), arr6, n);
		SortTestHelper.testSort("HeapSort1", new HeapSort1(), arr7, n);
		SortTestHelper.testSort("HeapSort2", new HeapSort2(), arr8, n);
		SortTestHelper.testSort("HeapSort", new HeapSort(), arr9, n);

	}
}
