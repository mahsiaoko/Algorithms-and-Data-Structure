package main.java.data_structure;

/**
 * @author Wrb
 * @date 2019/6/13 10:40
 */
//路径压缩优化
public class UnionFind5 {

	private int[] parent;
	private int[] rank; //高度
	private int count;

	public UnionFind5(int n) {
		count = n;
		parent = new int[n];
		rank = new int[n];
		for (int i = 0; i < n; i++) {
			parent[i] = i;
			rank[i] = 1;
		}
	}

	public int find(int p) {
		assert (p >= 0 && p < count);
		//路径压缩优化1（将当前指向指向父节点的父节点。跨两级）
//		while (p != parent[p]) {
//			parent[p] = parent[parent[p]];
//			p = parent[p];
//		}
//		return p;
		//路径压缩优化2(每个节点只有两级，理论最优，实际比1要差一点)
		if (p != parent[p]) {
			parent[p] = find(parent[p]);
		}
		return parent[p];
	}

	public boolean isConnected(int p, int q) {
		return find(p) == find(q);
	}

	public void unionElements(int p, int q) {
		int pRoot = find(p);
		int qRoot = find(q);

		if (pRoot == qRoot) {
			return;
		}
		if (rank[pRoot] < rank[qRoot]) {
			parent[pRoot] = qRoot;
		} else if (rank[qRoot] < rank[pRoot]) {
			parent[qRoot] = pRoot;
		}else {
			parent[pRoot] = qRoot;
			rank[qRoot] += 1;
		}
	}
}
