package main.java;

import main.java.algorithms.Components;
import main.java.algorithms.Path;
import main.java.utils.ReadGraph;
import main.java.data_structure.DenseGraph;
import main.java.data_structure.SparseGraph;

import java.util.Random;

/**
 * @author Wrb
 * @date 2019/6/13 14:18
 */
public class GraphMain {

	public static void main(String[] args) {
		int N = 20;
		int M = 100;

		SparseGraph sparseGraph = new SparseGraph(20, false);
		DenseGraph denseGraph = new DenseGraph(20, false);
		Random random = new Random();
		for (int i = 0; i < M; i++) {
			int a = random.nextInt(N);
			int b = random.nextInt(N);
			sparseGraph.addEdge(a, b);
			denseGraph.addEdge(a, b);
		}

//		sparseGraph.show();
//		System.out.println("-------------------------------------------");
//		denseGraph.show();

		// 使用两种图的存储方式读取testG1.txt文件
		String filename = "testG1.txt";
		SparseGraph g1 = new SparseGraph(13, false);
		ReadGraph readGraph1 = new ReadGraph(g1, filename);
		System.out.println("test G1 in Sparse Graph:");
//		g1.show();
		//连通分量
		Components component1 = new Components(g1);
		System.out.println("TestG1.txt, Component Count: " + component1.count());

		System.out.println();

		DenseGraph g2 = new DenseGraph(13, false);
		ReadGraph readGraph2 = new ReadGraph(g2 , filename );
		System.out.println("test G1 in Dense Graph:");
//		g2.show();

		System.out.println();

		// 使用两种图的存储方式读取testG2.txt文件
		filename = "testG2.txt";
		SparseGraph g3 = new SparseGraph(6, false);
		ReadGraph readGraph3 = new ReadGraph(g3, filename);
		System.out.println("test G2 in Sparse Graph:");
//		g3.show();

		Components component2 = new Components(g3);
		System.out.println("TestG2.txt, Component Count: " + component2.count());
		System.out.println();

		DenseGraph g4 = new DenseGraph(6, false);
		ReadGraph readGraph4 = new ReadGraph(g4, filename);
		System.out.println("test G2 in Dense Graph:");
//		g4.show();

		//寻路算法
		filename = "testG.txt";
		SparseGraph g = new SparseGraph(7, false);
		ReadGraph readGraph = new ReadGraph(g, filename);
		g.show();
		System.out.println();

		Path path = new Path(g,0);
		System.out.println("Path from 0 to 6 : ");
		path.showPath(6);

	}
}
